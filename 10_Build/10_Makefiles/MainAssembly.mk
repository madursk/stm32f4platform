#Set project directory
PROJDIR :=M:

#set project bsw sub modules
BSW_SUBMOD1 = Startup
BSW_SUBMOD2 = MCAL
BSW_SUBMOD3 = HAL
BSW_SUBMOD4 = MAL

#set subdirectories
BSW_SOURCE := $(PROJDIR)/20_BSW
APP_SOURCE := $(PROJDIR)/30_APP

#set project name
PORJNAME = stm32f4Platform

#Set Object path
OBJ_DIR = $(PROJDIR)/10_build/20_Results/objects

#Set binary path 
BIN_DIR =  $(PROJDIR)/10_build/20_Results/bin
#Set final executable name
TARGET = $(BIN_DIR)/$(PORJNAME).elf

#Set source files
SOURCE_FILES := $(BSW_SOURCE)/$(BSW_SUBMOD1)/ApplStart.c
#				$(BSW_SOURCE)/$(BSW_SUBMOD1)/LowLevelInit.c \
#				$(BSW_SOURCE)/$(BSW_SUBMOD2)/UART.c \
#				$(BSW_SOURCE)/$(BSW_SUBMOD2)/Port.c \
#				$(BSW_SOURCE)/$(BSW_SUBMOD2)/I2C.c \
#				$(BSW_SOURCE)/$(BSW_SUBMOD2)/I2C_cfg.c \
#				$(BSW_SOURCE)/$(BSW_SUBMOD2)/Port_cfg.c \
#				$(BSW_SOURCE)/$(BSW_SUBMOD3)/EcuM.c \
#				$(BSW_SOURCE)/$(BSW_SUBMOD3)/DiagHandler.c\
#				$(BSW_SOURCE)/$(BSW_SUBMOD3)/DiagHandler_cfg.c\
#				$(BSW_SOURCE)/$(BSW_SUBMOD3)/IoHandler.c \
#				$(BSW_SOURCE)/$(BSW_SUBMOD4)/BswM.c \
#				$(BSW_SOURCE)/$(BSW_SUBMOD1)/Main.c \
#				$(BSW_SOURCE)/$(BSW_SUBMOD1)/Utility.s

SOURCE_DIR := $(BSW_SOURCE)/$(BSW_SUBMOD1)

SOURCE_DIR2 := $(BSW_SOURCE)/$(BSW_SUBMOD2)

SOURCE_DIR3 := $(BSW_SOURCE)/$(BSW_SUBMOD3)

SOURCE_DIR4 := $(BSW_SOURCE)/$(BSW_SUBMOD4)

#set Linker Path
LINKER_PATH := $(BSW_SOURCE)/$(BSW_SUBMOD1)/PlatformB.ld

#Set Compiler 
CC = arm-none-eabi-gcc

#Set compilation flags
CFLAGS = -mcpu=cortex-m4 \
         -mthumb \
         -g3 \
         -o0 \
         -ffunction-sections\
         -fdata-sections \
         -fmessage-length=0 \
         -Wall \
         -gstrict-dwarf \
         -gdwarf-2 \
         -std=c99 \
       -mfloat-abi=hard \
       -mfpu=fpv4-sp-d16 

LFLAGS = -nostdlib -Xlinker -Map=$(BIN_DIR)/$(PORJNAME).map -T 

#Set directory to find dependencies
IDIR = -I$(BSW_SOURCE)/$(BSW_SUBMOD1)\
       -I$(BSW_SOURCE)/$(BSW_SUBMOD2)\
       -I$(BSW_SOURCE)/$(BSW_SUBMOD3)\
       -I$(BSW_SOURCE)/$(BSW_SUBMOD4)

CPATH = $(BSW_SOURCE)/$(BSW_SUBMOD1)\
       $(BSW_SOURCE)/$(BSW_SUBMOD2)\
       $(BSW_SOURCE)/$(BSW_SUBMOD3)\
       $(BSW_SOURCE)/$(BSW_SUBMOD4)

LD=arm-none-eabi-ld

All:$(OBJ_DIR)/ApplStart.o \
    $(OBJ_DIR)/Main.o
#    $(OBJ_DIR)/LowLevelInit.o \
#    $(OBJ_DIR)/BswM.o \
#    $(OBJ_DIR)/Os.o \
#    $(OBJ_DIR)/Os_isr.o \
#    $(OBJ_DIR)/Os_tasks.o \
#    $(OBJ_DIR)/DiagHandler.o \
#    $(OBJ_DIR)/I2C.o \
#    $(OBJ_DIR)/I2C_cfg.o \
#    $(OBJ_DIR)/I2C_irq.o \
#    $(OBJ_DIR)/NVIC.o \
#    $(OBJ_DIR)/NVIC_cfg.o \
#    $(OBJ_DIR)/UART.o \
#    $(OBJ_DIR)/UART_irq.o \
#    $(OBJ_DIR)/DiagHandler_cfg.o \
#    $(OBJ_DIR)/DevHandler.o \
#    $(OBJ_DIR)/DevHandler_cfg.o \
#    $(OBJ_DIR)/IoHandler.o \
#    $(OBJ_DIR)/EcuM.o \
#    $(OBJ_DIR)/Port.o \
#    $(OBJ_DIR)/Port_cfg.o \
#    $(OBJ_DIR)/EcuM.o\
#    $(OBJ_DIR)/Utility.o






#Rule to objects
$(OBJ_DIR)/ApplStart.o : $(SOURCE_DIR)/ApplStart.c
	$(CC) $(CFLAGS) $(IDIR) -c $< -o $@

#$(OBJ_DIR)/LowLevelInit.o : $(SOURCE_DIR)/LowLevelInit.c
#	$(CC) $(CFLAGS) $(IDIR) -c $< -o $@
#
#$(OBJ_DIR)/UART.o : $(SOURCE_DIR2)/UART.c
#	$(CC) $(CFLAGS) $(IDIR) -c $< -o $@
#
#$(OBJ_DIR)/UART_irq.o : $(SOURCE_DIR2)/UART_irq.c
#	$(CC) $(CFLAGS) $(IDIR) -c $< -o $@
#
#$(OBJ_DIR)/Port.o : $(SOURCE_DIR2)/Port.c
#	$(CC) $(CFLAGS) $(IDIR) -c $< -o $@
#
#$(OBJ_DIR)/I2C.o : $(SOURCE_DIR2)/I2C.c
#	$(CC) $(CFLAGS) $(IDIR) -c $< -o $@
#
#$(OBJ_DIR)/I2C_irq.o : $(SOURCE_DIR2)/I2C_irq.c
#	$(CC) $(CFLAGS) $(IDIR) -c $< -o $@
#
#$(OBJ_DIR)/I2C_cfg.o : $(SOURCE_DIR2)/I2C_cfg.c
#	$(CC) $(CFLAGS) $(IDIR) -c $< -o $@
#
#$(OBJ_DIR)/NVIC.o : $(SOURCE_DIR2)/NVIC.c
#	$(CC) $(CFLAGS) $(IDIR) -c $< -o $@
#
#$(OBJ_DIR)/NVIC_cfg.o : $(SOURCE_DIR2)/NVIC_cfg.c
#	$(CC) $(CFLAGS) $(IDIR) -c $< -o $@
#
#$(OBJ_DIR)/Port_cfg.o : $(SOURCE_DIR2)/Port_cfg.c
#	$(CC) $(CFLAGS) $(IDIR) -c $< -o $@
#
#$(OBJ_DIR)/DiagHandler.o : $(SOURCE_DIR3)/DiagHandler.c
#	$(CC) $(CFLAGS) $(IDIR) -c $< -o $@
#
#$(OBJ_DIR)/DiagHandler_cfg.o : $(SOURCE_DIR3)/DiagHandler_cfg.c
#	$(CC) $(CFLAGS) $(IDIR) -c $< -o $@
#
#$(OBJ_DIR)/DevHandler.o : $(SOURCE_DIR3)/DevHandler.c
#	$(CC) $(CFLAGS) $(IDIR) -c $< -o $@
#
#$(OBJ_DIR)/DevHandler_cfg.o : $(SOURCE_DIR3)/DevHandler_cfg.c
#	$(CC) $(CFLAGS) $(IDIR) -c $< -o $@
#
#$(OBJ_DIR)/EcuM.o : $(SOURCE_DIR3)/EcuM.c
#	$(CC) $(CFLAGS) $(IDIR) -c $< -o $@
#
#$(OBJ_DIR)/IoHandler.o : $(SOURCE_DIR3)/IoHandler.c
#	$(CC) $(CFLAGS) $(IDIR) -c $< -o $@
#
#$(OBJ_DIR)/BswM.o : $(SOURCE_DIR4)/BswM.c
#	$(CC) $(CFLAGS) $(IDIR) -c $< -o $@
#
#$(OBJ_DIR)/Os.o : $(SOURCE_DIR4)/Os.c
#	$(CC) $(CFLAGS) $(IDIR) -c $< -o $@
#
#$(OBJ_DIR)/Os_isr.o : $(SOURCE_DIR4)/Os_isr.c
#	$(CC) $(CFLAGS) $(IDIR) -c $< -o $@
#
#$(OBJ_DIR)/Os_tasks.o : $(SOURCE_DIR4)/Os_tasks.c
#	$(CC) $(CFLAGS) $(IDIR) -c $< -o $@
#
$(OBJ_DIR)/Main.o : $(SOURCE_DIR)/Main.c
	$(CC) $(CFLAGS) $(IDIR) -c $< -o $@

#$(OBJ_DIR)/Utility.o : $(SOURCE_DIR)/Utility.s
#	$(CC) $(CFLAGS) $(IDIR) -c $< -o $@