#Set project directory
PROJDIR :=M:

#set project bsw sub modules
BSW_SUBMOD1 = Startup
BSW_SUBMOD2 = MCAL
BSW_SUBMOD3 = HAL
BSW_SUBMOD4 = MAL

#set subdirectories
BSW_SOURCE := $(PROJDIR)/20_BSW
APP_SOURCE := $(PROJDIR)/30_APP

#set project name
PORJNAME = stm32f4Platform

#Set Object path
OBJ_DIR = $(PROJDIR)/10_build/20_Results/objects

#Set binary path 
BIN_DIR =  $(PROJDIR)/10_build/20_Results/bin
#Set final executable name
TARGET = $(BIN_DIR)/$(PORJNAME).elf

#Set source files
SOURCE_FILES := $(BSW_SOURCE)/$(BSW_SUBMOD1)/ApplStart.c
#				$(BSW_SOURCE)/$(BSW_SUBMOD1)/LowLevelInit.c \
#				$(BSW_SOURCE)/$(BSW_SUBMOD2)/UART.c \
#				$(BSW_SOURCE)/$(BSW_SUBMOD2)/UART_irq.c \
#				$(BSW_SOURCE)/$(BSW_SUBMOD2)/Port.c \
#				$(BSW_SOURCE)/$(BSW_SUBMOD2)/I2C.c \
#				$(BSW_SOURCE)/$(BSW_SUBMOD2)/I2C_cfg.c \
#				$(BSW_SOURCE)/$(BSW_SUBMOD2)/I2C_irq.c \
#				$(BSW_SOURCE)/$(BSW_SUBMOD2)/NVIC.c \
#				$(BSW_SOURCE)/$(BSW_SUBMOD2)/NVIC_cfg.c \
#				$(BSW_SOURCE)/$(BSW_SUBMOD2)/Port_cfg.c \
#				$(BSW_SOURCE)/$(BSW_SUBMOD3)/EcuM.c \
#				$(BSW_SOURCE)/$(BSW_SUBMOD3)/DiagHandler.c\
#				$(BSW_SOURCE)/$(BSW_SUBMOD3)/DiagHandler_cfg.c\
#				$(BSW_SOURCE)/$(BSW_SUBMOD3)/DevHandler.c\
#				$(BSW_SOURCE)/$(BSW_SUBMOD3)/DevHandler_Cfg.c\
#				$(BSW_SOURCE)/$(BSW_SUBMOD3)/IoHandler.c \
#				$(BSW_SOURCE)/$(BSW_SUBMOD4)/BswM.c \
#				$(BSW_SOURCE)/$(BSW_SUBMOD4)/Os.c \
#				$(BSW_SOURCE)/$(BSW_SUBMOD4)/Os_isr.c \
#				$(BSW_SOURCE)/$(BSW_SUBMOD4)/Os_tasks.c \
#				$(BSW_SOURCE)/$(BSW_SUBMOD1)/Main.c\
#				$(BSW_SOURCE)/$(BSW_SUBMOD1)/Utility.s

SOURCE_DIR := $(BSW_SOURCE)/$(BSW_SUBMOD1)

SOURCE_DIR2 := $(BSW_SOURCE)/$(BSW_SUBMOD2)

SOURCE_DIR3 := $(BSW_SOURCE)/$(BSW_SUBMOD3)

SOURCE_DIR4 := $(BSW_SOURCE)/$(BSW_SUBMOD4)

#set Linker Path
LINKER_PATH := $(BSW_SOURCE)/$(BSW_SUBMOD1)/PlatformB.ld

#Set Compiler 
CC = arm-none-eabi-gcc

#Set compilation flags
CFLAGS = -mcpu=cortex-m4 \
         -mthumb \
         -g3 \
         -o0 \
         -ffunction-sections\
         -fdata-sections \
         -fmessage-length=0 \
         -Wall \
         -gstrict-dwarf \
         -gdwarf-2 \
         -std=c99
#        -mfloat-abi=hard \
#        -mfpu=fpv4-sp-d16 \

LFLAGS = -nostdlib -Xlinker -Map=$(BIN_DIR)/$(PORJNAME).map -T 

#OBJ = $(patsubst $(SOURCE_DIR)/%.c,$(OBJ_DIR)/%.o,$(SOURCE_FILES))

#Set directory to find dependencies
IDIR = $(BSW_SOURCE)/$(BSW_SUBMOD1)\
       $(BSW_SOURCE)/$(BSW_SUBMOD2)\
       $(BSW_SOURCE)/$(BSW_SUBMOD3)\
       $(BSW_SOURCE)/$(BSW_SUBMOD4)

LD=arm-none-eabi-ld

# #Rule to compile
$(TARGET) : $(SOURCE_FILES)
	$(CC) $(CFLAGS) $(SOURCE_FILES) -o $(TARGET) $(LFLAGS) $(LINKER_PATH) -Xlinker --gc-sections -Xlinker -print-memory-usage -Xlinker --sort-section=alignment -Xlinker --cref

Clean:
	rm -f $(OBJ_DIR)/*.o *.elf *.map 
	rm -f $(BIN_DIR)/*.elf $(BIN_DIR)/*.map
	echo Clean done