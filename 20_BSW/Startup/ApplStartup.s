    .text
    .code 16
    .syntax unified

    .global u32VectorTable
    .global Reset_Handler

 /* start address for the .data section. defined in linker script */
.word _sdata
/* end address for the .data section. defined in linker script */
.word _edata
/* start address for the .bss section. defined in linker script */
.word _sbss
/* end address for the .bss section. defined in linker script */
.word _ebss
