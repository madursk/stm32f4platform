    .text
    .code 16
    .syntax unified

    .global _EnableFPU
    .func   _EnableFPU

_EnableFPU:
    LDR.w   R0, =0xE000ED88       // CPACR is located at address 0xE000ED88
    LDR     R1, [R0]              // Read CPACR
    ORR     R1, R1, #(0xF << 20)  // Set bits 20-23 to enable CP10 and CP11 coprocessors
    STR     R1, [R0]              // Write back the modified value to the CPACR
    DSB                           // Wait for store to complete
    ISB                           // Reset pipeline now the FPU is enabled
    MOV pc,lr
    .endfunc

    .global _SetPrivilege
    .func   _SetPrivilege
_SetPrivilege:
    MRS     R0, CONTROL            // Load CONTROL Register to Register R0
    ORR     R0, R0, #(0x01)        // Set nPRIV bit te set privileged status
    MSR   CONTROL,R0               // Load R0 to CONTROL register
    DSB                            // Wait for store to complete
    ISB                           // Reset pipeline 
    MOV pc,lr
.endfunc

   .global _SetPendSV
   .func   _SetPendSV
_SetPendSV:
    LDR.w   R0, =0xE000ED04       // PendSrv  is located at address 0xE000ED04
    LDR     R1, [R0]              // Read ICSR
    ORR     R1, R1, #(0x1 << 28)  // Set bit 28 to enable Set PendSrv bit
    STR     R1, [R0]              // Write back the modified value to the ICSR
    DSB                           // Wait for store to complete
    ISB                           // Reset pipeline
    MOV pc,lr
.endfunc
