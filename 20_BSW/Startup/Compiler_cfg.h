#ifndef COMPILER_CFG_HEADER_H
#define COMPILER_CFG_HEADER_H

#define STACK_VAR __attribute__((section(".data")))
#define AUTOMATIC __attribute__((section(".text")))
//#define OS_TASK_INIT_TCB __attribute__((section(".TaskInitTCB")))
//#define OS_TASK_BSW_TCB __attribute__((section(".TaskBswTCB")))
//#define OS_TASK_APP_TCB __attribute__((section(".TaskAppTCB")))
//#define OS_TASK_IDLE_TCB __attribute__((section(".TaskIdleTCB")))
#endif
