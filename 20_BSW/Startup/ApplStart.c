#include "Datatypes.h"
#include "ApplStart.h"
//#include "Os_tasks.h"
//extern void Os_ErrorHook(void);
/*                      ARM Core Exceptions                                         */
//extern uint32_t* _stext;
//extern uint32_t* _etext;
extern uint32_t _sdata;
extern uint32_t _edata;
extern uint32_t _sbss;
extern uint32_t _ebss;
extern uint32_t _estack;
uint32_t ua32BssDebugVar[20];
uint32_t ua32DataDebugVar = 285;

void Reset_Handler();
void NMI_Handler() __attribute__((weak,alias("Default_Handler")));
void HardFault_Handler() __attribute__((weak,alias("Default_Handler")));
void MemManage_Handler() __attribute__((weak,alias("Default_Handler")));
void BusFault_Handler() __attribute__((weak,alias("Default_Handler")));
void UsageFault_Handler() __attribute__((weak,alias("Default_Handler")));
void SVC_Handler() __attribute__((weak,alias("Default_Handler")));
void DebugMon_Handler() __attribute__((weak,alias("Default_Handler")));
void PendSV_Handler() __attribute__((weak,alias("Default_Handler")));
void SysTick_Handler() __attribute__((weak,alias("Default_Handler")));
void WWDG_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void PVD_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void TAMP_STAMP_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void RTC_WKUP_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void RCC_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void EXTI0_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void EXTI1_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void EXTI2_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void EXTI3_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void EXTI4_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void DMA1_Stream0_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void DMA1_Stream1_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void DMA1_Stream2_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void DMA1_Stream3_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void DMA1_Stream4_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void DMA1_Stream5_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void DMA1_Stream6_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void ADC_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void CAN1_TX_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void CAN1_RX0_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void CAN1_RX1_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void CAN1_SCE_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void EXTI9_5_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void TIM1_BRK_TIM9_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void TIM1_UP_TIM10_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void TIM1_TRG_COM_TIM11_IRQHan() __attribute__((weak,alias("Default_Handler")));
void TIM1_CC_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void TIM2_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void TIM3_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void TIM4_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void I2C1_EV_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void I2C1_ER_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void I2C2_EV_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void I2C2_ER_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void SPI1_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void SPI2_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void USART1_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void USART2_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void USART3_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void EXTI15_10_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void RTC_Alarm_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void OTG_FS_WKUP_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void TIM8_BRK_TIM12_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void TIM8_UP_TIM13_IRQHandler () __attribute__((weak,alias("Default_Handler")));
void TIM8_TRG_COM_TIM14_IRQHan() __attribute__((weak,alias("Default_Handler")));
void TIM8_CC_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void DMA1_Stream7_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void FSMC_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void SDIO_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void TIM5_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void SPI3_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void UART4_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void UART5_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void TIM6_DAC_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void TIM7_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void DMA2_Stream0_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void DMA2_Stream1_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void DMA2_Stream2_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void DMA2_Stream3_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void DMA2_Stream4_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void ETH_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void ETH_WKUP_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void CAN2_TX_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void CAN2_RX0_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void CAN2_RX1_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void CAN2_SCE_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void OTG_FS_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void DMA2_Stream5_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void DMA2_Stream6_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void DMA2_Stream7_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void USART6_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void I2C3_EV_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void I2C3_ER_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void OTG_HS_EP1_OUT_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void OTG_HS_EP1_IN_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void OTG_HS_WKUP_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void OTG_HS_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void DCMI_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void CRYP_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void HASH_RNG_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void FPU_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void LCD_TFT_IRQHandler() __attribute__((weak,alias("Default_Handler")));
void LCD_TFT_1_IRQHandler() __attribute__((weak,alias("Default_Handler")));

uint32_t vectors[] __attribute__((section(".isr_vector"))) =
{
	&_estack,
	Reset_Handler,
	NMI_Handler,
	HardFault_Handler,
	MemManage_Handler,
	BusFault_Handler,
	UsageFault_Handler,
	0,
	0,
	0,
	0,
	SVC_Handler,
	DebugMon_Handler,
	0,
	PendSV_Handler,
	SysTick_Handler,
	WWDG_IRQHandler,
	PVD_IRQHandler,
	TAMP_STAMP_IRQHandler,
	RTC_WKUP_IRQHandler,
	0,
	RCC_IRQHandler,
	EXTI0_IRQHandler,
	EXTI1_IRQHandler,
	EXTI2_IRQHandler,
	EXTI3_IRQHandler,
	EXTI4_IRQHandler,
	DMA1_Stream0_IRQHandler,
	DMA1_Stream1_IRQHandler,
	DMA1_Stream2_IRQHandler,
	DMA1_Stream3_IRQHandler,
	DMA1_Stream4_IRQHandler,
	DMA1_Stream5_IRQHandler,
	DMA1_Stream6_IRQHandler,
	ADC_IRQHandler,
	CAN1_TX_IRQHandler,
	CAN1_RX0_IRQHandler,
	CAN1_RX1_IRQHandler,
	CAN1_SCE_IRQHandler,
	EXTI9_5_IRQHandler,
	TIM1_BRK_TIM9_IRQHandler,
	TIM1_UP_TIM10_IRQHandler,
	TIM1_TRG_COM_TIM11_IRQHan,
	TIM1_CC_IRQHandler,
	TIM2_IRQHandler,
	TIM3_IRQHandler,
	TIM4_IRQHandler,
	I2C1_EV_IRQHandler,
	I2C1_ER_IRQHandler,
	I2C2_EV_IRQHandler,
	I2C2_ER_IRQHandler,
	SPI1_IRQHandler,
	SPI2_IRQHandler,
	USART1_IRQHandler,
	USART2_IRQHandler,
	USART3_IRQHandler,
	EXTI15_10_IRQHandler,
	RTC_Alarm_IRQHandler,
	OTG_FS_WKUP_IRQHandler,
	TIM8_BRK_TIM12_IRQHandler,
	TIM8_UP_TIM13_IRQHandler ,
	TIM8_TRG_COM_TIM14_IRQHan,
	TIM8_CC_IRQHandler,
	DMA1_Stream7_IRQHandler,
	FSMC_IRQHandler,
	SDIO_IRQHandler,
	TIM5_IRQHandler,
	SPI3_IRQHandler,
	UART4_IRQHandler,
	UART5_IRQHandler,
	TIM6_DAC_IRQHandler,
	TIM7_IRQHandler,
	DMA2_Stream0_IRQHandler,
	DMA2_Stream1_IRQHandler,
	DMA2_Stream2_IRQHandler,
	DMA2_Stream3_IRQHandler,
	DMA2_Stream4_IRQHandler,
	ETH_IRQHandler,
	ETH_WKUP_IRQHandler,
	CAN2_TX_IRQHandler,
	CAN2_RX0_IRQHandler,
	CAN2_RX1_IRQHandler,
	CAN2_SCE_IRQHandler,
	OTG_FS_IRQHandler,
	DMA2_Stream5_IRQHandler,
	DMA2_Stream6_IRQHandler,
	DMA2_Stream7_IRQHandler,
	USART6_IRQHandler,
	I2C3_EV_IRQHandler,
	I2C3_ER_IRQHandler,
	OTG_HS_EP1_OUT_IRQHandler,
	OTG_HS_EP1_IN_IRQHandler,
	OTG_HS_WKUP_IRQHandler,
	OTG_HS_IRQHandler,
	DCMI_IRQHandler,
	CRYP_IRQHandler,
	HASH_RNG_IRQHandler,
	FPU_IRQHandler,
	0,
	0,
	0,
	0,
	0,
	0,
	LCD_TFT_IRQHandler,
	LCD_TFT_1_IRQHandler
};

//volatile uint32_t* romData;
//extern volatile uint32_t* _taskInitStart;
//extern volatile uint32_t* _taskInitEnd;
//extern volatile uint32_t* _taskBswStart;
//extern volatile uint32_t* _taskBswEnd;
//extern volatile uint32_t* _taskApplStart;
//extern volatile uint32_t* _taskApplEnd;
//extern volatile uint32_t* _taskIdleStart;
//extern volatile uint32_t* _taskIdleEnd;
//extern volatile uint32_t* taskInitStart = &_taskInitStart;
//extern volatile uint32_t* taskBswStart = &_taskBswStart;
//extern volatile uint32_t* taskApplStart = &_taskApplStart;
//extern volatile uint32_t* taskIdleStart = &_taskIdleStart;
////extern volatile TaskControlBlock TCB_initTsk OS_TASK_INIT_TCB;
//extern volatile TaskControlBlock TCB_bswTsk OS_TASK_BSW_TCB;
//extern volatile TaskControlBlock TCB_ApplTsk OS_TASK_APP_TCB;
//extern volatile TaskControlBlock TCB_IdleTsk OS_TASK_IDLE_TCB;

void Reset_Handler(void)
{
	uint32_t u32DebugCounter = 0;
	/*clear BSS*/
	for(uint32_t *bssTempPtr = (uint32_t*)&_sbss; bssTempPtr < (uint32_t *)&_ebss;)
	{
		*bssTempPtr++ = 0;
	     u32DebugCounter++;
	}

    /*fill stack with 0xAAAAAAAA pattern*/
   for (uint32_t *stackMemPtr = (uint32_t*)&_ebss; stackMemPtr < (uint32_t*) &_estack;)
    {
        *stackMemPtr++ = STACK_PATTERN;
    }

   
//   /*Change main stack pointer with the process stack pointer*/
//    __asm__ volatile ("LDR r0, =0x2002FFFF"::);
//    __asm__ volatile("msr psp, r0"::);
//    __asm__ volatile("ISB"::);
//    __asm__ volatile("MRS R0, CONTROL"::);
//    __asm__ volatile("ORR R0, R0, #0x2"::);
//    __asm__ volatile("MSR CONTROL, R0"::);
//    __asm__ volatile("ISB"::);
//    /*START of initialization of inital stack frames of tasks*/
//    __asm__ volatile("MOV r3, sp"::); /*Store stack pointer before manipulating tasks stack pointers*/
//    /*Task Init stack frame*/
//    __asm__ volatile("LDR r0,=taskInitStart"::);
//    __asm__ volatile("LDR r1, [r0]"::);
//    __asm__ volatile("LDR r2,=TASK_INIT"::);
//    __asm__ volatile("MOV r7, r1"::);
//    __asm__ volatile("MOV sp, r1"::);
//    __asm__ volatile("MOV lr, r2"::);
//    __asm__ volatile("STMDB sp!, {r4-r11, r14}"::);
//    __asm__ volatile("STR sp, [r0]"::);
//    TCB_initTsk.StackTop = taskInitStart;
//    /*Task Bsw stack frame*/
//    __asm__ volatile("LDR r0,=taskBswStart"::);
//    __asm__ volatile("LDR r1, [r0]"::);
//    __asm__ volatile("LDR r2,=TASK_BSW"::);
//    __asm__ volatile("MOV r7, r1"::);
//    __asm__ volatile("MOV sp, r1"::);
//    __asm__ volatile("MOV lr, r2"::);
//    __asm__ volatile("STMDB sp!, {r4-r11, r14}"::);
//    __asm__ volatile("STR sp, [r0]"::);
//    TCB_bswTsk.StackTop = taskBswStart;
//    /*Task Appl stack frame*/
//    __asm__ volatile("LDR r0,=taskApplStart"::);
//    __asm__ volatile("LDR r1, [r0]"::);
//    __asm__ volatile("LDR r2,=TASK_APPL"::);
//    __asm__ volatile("MOV r7, r1"::);
//    __asm__ volatile("MOV sp, r1"::);
//    __asm__ volatile("MOV lr, r2"::);
//    __asm__ volatile("STMDB sp!, {r4-r11, r14}"::);
//    __asm__ volatile("STR sp, [r0]"::);
//    TCB_ApplTsk.StackTop = taskApplStart;
//    /*Task Idle stakc frame*/
//    __asm__ volatile("LDR r0,=taskIdleStart"::);
//    __asm__ volatile("LDR r1, [r0]"::);
//    __asm__ volatile("LDR r2,=TASK_IDLE"::);
//    __asm__ volatile("MOV r7, r1"::);
//    __asm__ volatile("MOV sp, r1"::);
//    __asm__ volatile("MOV lr, r2"::);
//    __asm__ volatile("STMDB sp!, {r4-r11, r14}"::);
//    __asm__ volatile("STR sp, [r0]"::);
//    TCB_IdleTsk.StackTop = taskIdleStart;
    /*END of initialization of inital stack frames of tasks*/ 
//    __asm__ volatile("MOV sp, r3"::); /*Restore stack pointer after manipulating tasks stack pointers*/
    main(); /*jump to main*/
    while(1); /*code should never reach here*/
}

void Default_Handler(void)
{
    __asm("bkpt");
}

//void NMI_Handler(void)
//{
// __asm("bkpt");
//}
//void HardFault_Handler(void)
//{
// __asm("bkpt");
//}
//void MemManageFault_Handler(void)
//{
//  __asm("bkpt");
//}
//void BusFault_Handler(void)
//{
//  __asm("bkpt");
//}
//void UsageFault_Handler(void)
//{
// __asm("bkpt");
//}
//
//void DebugMon_Handler(void)
//{
// __asm("bkpt");
//}


// void DMA_Channel0_IRQHandler(void)
// {
//      __asm("bkpt");
// }

// void DMA_Channel1_IRQHandler(void)
// {
//      __asm("bkpt");
// }

// void DMA_Channel2_IRQHandler(void)       
// {
//      __asm("bkpt");
// }

// void DMA_Channel3_IRQHandler(void)       
// {
//      __asm("bkpt");
// }

// void DMA_Channel4_IRQHandler(void)       
// {
//      __asm("bkpt");
// }

// void DMA_Channel5_IRQHandler(void)       
// {
//  __asm("bkpt");
// }

// void DMA_Channel6_IRQHandler(void)       
// {
//  __asm("bkpt");
// }

// void DMA_Channel7_IRQHandler(void)       
// {
//  __asm("bkpt");
// }

// void DMA_Channel8_IRQHandler(void)       
// {
//  __asm("bkpt");
// }

// void DMA_Channel9_IRQHandler(void)       
// {
//  __asm("bkpt");
// }

// void DMA_Channel10_IRQHandler(void)      
// {
//      __asm("bkpt");
// }

// void DMA_Channel11_IRQHandler(void)      
// {
//      __asm("bkpt");
// }

// void DMA_Channel12_IRQHandler(void)      
// {
//      __asm("bkpt");
// }
// void DMA_Channel13_IRQHandler(void)      
// {
//      __asm("bkpt");
// }
// void DMA_Channel14_IRQHandler(void)      
// {
//      __asm("bkpt");
// }
// void DMA_Channel15_IRQHandler(void)      
// {
//      __asm("bkpt");
// }
// void DMA_Error_IRQHandler(void)          
// {
//      __asm("bkpt");
// }
// void MCM_IRQHandler(void)                
// {
//      __asm("bkpt");
// }
// void FLS_CC_IRQHandler(void)             
// {
//      __asm("bkpt");
// }
// void FLS_RC_IRQHandler(void)             
// {
//      __asm("bkpt");
// }
// void MODCON_LV_IRQHandler(void)          
// {
//      __asm("bkpt");
// }
// void LLWU_IRQHandler(void)               
// {
//      __asm("bkpt");
// }
// void WDG_IRQHandler(void)                
// {
//      __asm("bkpt");
// }
// void CRY_RNG_IRQHandler(void)            
// {
//      __asm("bkpt");
// }

// void I2C1_IRQHandler(void)               
// {
//      __asm("bkpt");
// }
// void SPI0_IRQHandler(void)               
// {
//      __asm("bkpt");
// }
// void SPI1_IRQHandler(void)               
// {
//      __asm("bkpt");
// }
// void I2S0_TX_IRQHandler(void)            
// {
//      __asm("bkpt");
// }
// void I2S0_RX_IRQHandler(void)            
// {
//      __asm("bkpt");
// }
// void UART0_IRQHandler(void)              
// {
//      __asm("bkpt");
// }
// void UART0_Error_IRQHandler(void)        
// {
//      __asm("bkpt");
// }
// void UART1_IRQHandler(void)              
// {
//      __asm("bkpt");
// }
// void UART1_Error_IRQHandler(void)        
// {
//      __asm("bkpt");
// }
// void UART2_IRQHandler(void)              
// {
//      __asm("bkpt");
// }
// void UART2_Error_IRQHandler(void)        
// {
//      __asm("bkpt");
// }
// void UART3_Error_IRQHandler(void)        
// {
//      __asm("bkpt");
// }
// void ADC0_IRQHandler(void)               
// {
//      __asm("bkpt");
// }
// void CMP0_IRQHandler(void)               
// {
//      __asm("bkpt");
// }
// void CMP1_IRQHandler(void)               
// {
//      __asm("bkpt");
// }
// void FTM0_IRQHandler(void)               
// {
//      __asm("bkpt");
// }
// void FTM1_IRQHandler(void)               
// {
//      __asm("bkpt");
// }
// void FTM2_IRQHandler(void)               
// {
//      __asm("bkpt");
// }
// void CMT_IRQHandler(void)                
// {
//      __asm("bkpt");
// }
// void RTC_Alarm_IRQHanlder(void)          
// {
//      __asm("bkpt");
// }
// void RTC_Sec_IRQHandler(void)            
// {
//      __asm("bkpt");
// }
// void PIT0_IRQHandler(void)               
// {
//      __asm("bkpt");
// }
// void PIT1_IRQHandler(void)               
// {
//      __asm("bkpt");
// }
// void PIT2_IRQHandler(void)               
// {
//      __asm("bkpt");
// }
// void PIT3_IRQHandler(void)               
// {
//      __asm("bkpt");
// }
// void PTB_IRQHandler(void)                
// {
//      __asm("bkpt");
// }
// void USB_OTG_IRQHandler(void)            
// {
//      __asm("bkpt");
// }
// void USB_CHRG_IRQHandler(void)           
// {
//      __asm("bkpt");
// }
// void DAC0_IRQHandler(void)               
// {
//      __asm("bkpt");
// }
// void MCG_IRQHandler(void)                
// {
//      __asm("bkpt");
// }
// void LP_TIMER_IRQHandler(void)           
// {
//      __asm("bkpt");
// }
// void PCMA_IRQHandler(void)               
// {
//      __asm("bkpt");
// }
// void PCMB_IRQHandler(void)               
// {
//      __asm("bkpt");
// }
// void PCMC_IRQHandler(void)            
// {
//      __asm("bkpt");
// }
// void PCMD_IRQHandler(void)              
// {
//      __asm("bkpt");
// }
// void PCME_IRQHandler(void)         
// {
//      __asm("bkpt");
// }
// void SW_IRQHandler(void)             
// {
//      __asm("bkpt");
// }
// void SPI2_IRQHandler(void)             
// {
//      __asm("bkpt");
// }
// void UART4_IRQHandler(void)             
// {
//      __asm("bkpt");
// }
// void UART4_Error_IRQHandler(void)       
// {
//      __asm("bkpt");
// }
// void UART5_IRQHandler(void)           
// {
//      __asm("bkpt");
// }
// void UART5_Error_IRQHandler(void)      
// {
//      __asm("bkpt");
// }
// void CMP2_IRQHandler(void)            
// {
//      __asm("bkpt");
// }
// void FTM3_IRQHandler(void)             
// {
//      __asm("bkpt");
// }
// void DAC1_IRQHandler(void)            
// {
//      __asm("bkpt");
// }
// void ADC1_IRQHandler(void)              
// {
//      __asm("bkpt");
// }
// void I2C2_IRQHandler(void)               
// {
//   __asm("bkpt");
// }
// void CAN0_BUFF_IRQHandler(void)       
// {
//  __asm("bkpt");
// }
// void CAN0_BUSOFF_IRQHandler(void)       
// {
//  __asm("bkpt");
// }
// void CAN0_Error_IRQHandler(void)        
// {
//  __asm("bkpt");
// }
// void CAN0_TXW_IRQHandler(void)          
// {
//  __asm("bkpt");
// }
// void CAN0_RXW_IRQHandler(void)          
// {
//  __asm("bkpt");
// }
// void CAN0_WU_IRQHandler(void)          
// {
//  __asm("bkpt");
// }
// void SDHC_IRQHandler(void)
// {
//     __asm("bkpt");
// }
// void ETH_TMR_IRQHandler(void)
// {
//     __asm("bkpt");
// }
// void ETH_TX_IRQHandler(void)            
// {
//  __asm("bkpt");
// } 

// void ETH_RX_IRQHandler(void)            
// {
//  __asm("bkpt");
// }
// void ETH_Error_IRQHandler(void)          
// {
//  __asm("bkpt");
// }
